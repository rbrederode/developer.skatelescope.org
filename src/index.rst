.. developer.skatelescope.org documentation master file, created by
   sphinx-quickstart on Wed Dec 13 11:59:38 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Home
  :hidden:

SKA telescope developer portal
------------------------------

Welcome to the `Square Kilometre Array <http://www.skatelescope.org>`_ software
documentation portal. Whether you are a developer involved in SKA or you are
simply one of our many users, all of our software processes and projects are
documented in this portal.

Scope
=====

This documentation applies to the bridging phase of the SKA project, further
updates and scope changes will be published on this website.
Part of the bridging phase goals will be to consolidate and enrich this portal
with more detailed information. It is thus anticipated that in this phase
the change rate of the documentation will be very frequent.



SKA developer community
-----------------------

SKA software development is managed in an open and transparent way.

.. COMMUNITY SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Community
  :hidden:

  community/code_of_conduct
  community/getting_started
  community/teams_responsibilities
  community/decision_making

- :doc:`community/code_of_conduct`
- :doc:`community/getting_started`
- :doc:`community/teams_responsibilities`
- :doc:`community/decision_making`

A list of the tools we are using to collaborate, together with guidance on how to use them can be found at this confluence page: `SKA Guidelines to Remote Working <https://confluence.skatelescope.org/display/SKAIT/SKA+Guidelines+to+Remote+Working>`_ (requires an SKA Confluence account).


.. DEVELOPMENT TOOLS SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Development tools
  :hidden:

  tools/git
  tools/github2gitlab/github2gitlab
  tools/continuousintegration
  tools/cidashboard
  tools/tango-devenv-setup
  tools/pycharm/pycharm
  tools/vscode/vscode
  tools/jira

Development tools
-----------------
Working with git
================

Git is adopted as distributed version control system, and all SKA code shall be hosted in a git repository.
The gitlab organization *ska-telescope* can be found at https://gitlab.com/ska-telescope . All SKA developers
must have a gitlab account and be added to the organization as part of a team.

- :doc:`tools/git`

Working with SKA Jira
=====================

Every team is tracking daily work in a team-based project on our JIRA server at https://jira.skatelescope.org.

- :doc:`tools/jira`

.. todo::
   - Create a new project
   - Link to issue tracker


Development Environments
========================

Python and Tango development
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A completely configured development environment can be set up very easily. This will include 
TANGO, PyTANGO, docker and properly configured IDEs.

- :doc:`tools/tango-devenv-setup`


PyCharm and VSCode are two IDEs that can be configured to support python and 
PyTANGO development activities. You will find detailed instructions and how-tos at:

- :doc:`tools/pycharm/pycharm`
- :doc:`tools/vscode/vscode`


.. AGILE PRACTICES FOLLOWED AT SKA SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Agile practices followed at SKA
  :hidden:

  development_practices/ska_testing_policy_and_strategy
  development_practices/definition_of_done

Development practices followed at SKA
-------------------------------------

Testing policy and strategy
===========================

The SKA testing policy and strategy contains useful guidelines and practices to be 
followed when developing software for the SKA project.

- :doc:`development_practices/ska_testing_policy_and_strategy`

Definition of Done
==================

The definition of done is used to guide teams in planning and estimating the size of stories and features:

- :doc:`development_practices/definition_of_done`


.. DEVELOPMENT GUIDELINES SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Development guidelines
  :hidden:

  development/getting_started
  development/fundamental_sw_requirements
  development/python-codeguide
  development/javascript-codeguide
  development/vhdl-codeguide
  development/cplusplus-codeguide
  development/containerisation-standards
  development/orchestration-guidelines
  development/software_package_release_procedure
  development/uploading-docker-nexus
  development/logging-format
  development/gitlab-variables


Development guidelines
----------------------


Getting Started Guide
=====================

A pocket-guide to documentation on adding a new project, development, containerisation and integration.

- :doc:`development/getting_started`

Fundamental SKA Software & Hardware Description Language Standards
==================================================================

These standards underpin all SKA software development. The canonical copy is 
`held in eB <https://ska-aw.bentley.com/SKAProd/Search/QuickLink.aspx?n=SKA-TEL-SKO-0000661&t=3&d=&sc=Global&i=view>`_,
but the essential information is here:

- :doc:`development/fundamental_sw_requirements`


Python coding guidelines
========================

A Python skeleton project is created for use within the SKA Telescope. This skeleton purpose is to
enforce coding best practices and bootstrap the initial project setup. Any development should start
by forking this skeleton project and change the appropriate files.

- :doc:`development/python-codeguide`

Javascript coding guidelines
============================

A React based javascript skeleton project is created for use within the SKA Telescope. Similar to the 
Python skeleton above its purpose is to enforce coding best practices and bootstrap the initial project 
setup for browser based javascript applications.

- :doc:`development/javascript-codeguide`

VHDL coding guidelines
======================

VHDL coding guidelines are described at: 

- :doc:`development/vhdl-codeguide`


C++ Coding Standards
==========================

A CPP skeleton project is created for use within the SKA Telescope. The skeleton purpose is to demonstrate
coding best practices, boostrap initial project set up within the SKA Continuous Integration (CI) Framework.

- :doc:`development/cplusplus-codeguide`


Containerisation Standards
==========================

A set of standards, conventions and guidelines for building, integrating and maintaining Container
technologies.

- :doc:`development/containerisation-standards`

Container Orchestration Guidelines
==================================

A set of standards, conventions and guidelines for deploying application suites on Container Orchestration technologies.

- :doc:`development/orchestration-guidelines`

SKA Software Packaging Procedure
================================

This details a procedure that all *SKA* developers shall follow to ensure that they make use of the
existing CI/CD pipelines to automate the building of their software packages for release.

- :doc:`development/software_package_release_procedure`

Hosting a docker image on Nexus
===============================

This details steps that all *SKA* developers shall abide to when building and hosting their docker
images on the Nexus registry.

- :doc:`development/uploading-docker-nexus`

Logging Guidelines
==================

A standard logging format exists for logging in the evolutionary prototype into an ELK stack logging system designed for the SKA software.

- :doc:`development/logging-format`

GitLab Variables
================

Enumeration and description of the global variables defined on GitLab used in the
CI/CD infrastructure.

- :doc:`development/gitlab-variables`

.. PROJECTS SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Projects
  :hidden:

  projects/list
  projects/create_new_project
  projects/document_project
  projects/licensing

Projects
--------

- :doc:`projects/list`
- :doc:`projects/create_new_project`
- :doc:`projects/document_project`
- :doc:`projects/licensing`

.. SERVICES SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Services
  :hidden:

  services/ait_performance_env
  services/monitoring-dashboards


Developer Services
------------------

- :doc:`services/ait_performance_env`
- :doc:`services/monitoring-dashboards`

.. SHARE SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Share Your Knowledge
  :hidden:

  community/share_your_knowledge  


Share Your Knowledge
--------------------
- :doc:`community/share_your_knowledge`

Commitment to opensource
------------------------

As defined in SKA software standard, SKA software development is committed to opensource,
and an open source licensing model is always preferred within SKA software development.

.. todo::
   - Committment to opensource
   - Exceptions

.. RECOMMENDED READING SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Recommended Reading
  :hidden:

  recommended_reading/system_design
  recommended_reading/programming
  recommended_reading/programming_languages

.. FOLLOW US SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Follow us
  :hidden:

  follow_us/followus_env

Follow Us
----------

- :doc:`follow_us/followus_env`

